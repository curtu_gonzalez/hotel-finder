import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HotelService {

  constructor(private http: Http) { }

  getHotels(params){
    return this.http
      .get('http://localhost:3000/hotel', params)
      .pipe(map(response => {return response.json().rows;}))
      .toPromise();
  }

}
