import { Component, OnInit } from '@angular/core';
import {HotelService} from '../hotel.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  
  hotelName : string = '';
  hotels : any[]  =[];
  filteredHotels : any[]=[];
  ratings : number[] = [];
  allStars : boolean = true;
  fiveStars : boolean = false;
  fourStars : boolean = false;
  threeStars : boolean = false;
  twoStars : boolean = false;
  oneStars : boolean = false;


  constructor(private hotelService: HotelService) { }

  ngOnInit() {
    this.searchHotels();
  }

  //TODO refactor into to  simple component
  arrayOf(times: number): any[]{
    return Array(Number(times));
  }

  searchHotels(){
    //TODO validate name
    //this.validateHotelName

    let params = {
      name:this.hotelName
    };

    this.hotelService.getHotels({params})
      .then(results=>{
        this.hotels = results;
        this.filterByRating();
      })
      .catch(err=>{
        console.error(err);
      });
  }

  ratingChange(rating:number){
    console.log(this.ratings);
    let index = this.ratings.indexOf(rating);
    if(index<0){
      this.ratings.push(rating)
    } else {
      this.ratings.splice(index,1);
    }
    this.filterByRating();
  }

  filterByRating(){
    console.log('todos:'+this.allStars);
    if(this.allStars){
      this.filteredHotels = this.hotels;
    } else {
      let tempHotels = [];
      for(const hotel of this.hotels){
        console.log('ratings: '+this.ratings);
        console.log('index: '+this.ratings.indexOf(Number(hotel.rating)));

        if(this.ratings.indexOf(Number(hotel.rating))>=0)
          tempHotels.push(hotel);
      }
      this.filteredHotels = tempHotels;
    }
  }
}
