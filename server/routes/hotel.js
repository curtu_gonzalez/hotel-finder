const db = require('../db/index.js');
const util = require('util');

function getHotels(request,response){
  let query = "select * from hotel" +
                " where "+
                " name ilike '%s'";
                //TODO filter on server for any value
                /*
                " and price between @minPrice@ and @maxPrice@"+
                " and rating between @minRating@ and @maxRating@"
                " and amenities @>'{}'::varchar[]';";
                */

  query = util.format(query,'%'+request.query.name+'%');
  console.log(query);
  return db.query(query)
          .then((res) => {
            response.status(200).send({rows:res.rows});
          })
          .catch(err =>{
            console.error('Something went wrong '+err);
            response.status(500).send({ error: 'Service failed, please try again later' });
          })
}

module.exports = {
  get:getHotels,
}
