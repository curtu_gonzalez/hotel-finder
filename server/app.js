const express = require('express');
const app = express();
const config = require('./config');

//easy json body for the services
app.use(express.json());
app.use (function (error, req, res, next){
  //we have a bad request and will broke the service so inform the error
  if(error){
    res.sendStatus(400);
    return;
  }
  //the request is ok continue
  next();
});

// TODO ADD IF CORS IS NEEDED
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//modules with 
const hotel = require('./routes/hotel.js');



//it's working
app.get('/', (req, res) =>res.send('Yes another rest API... oh HELLO!!!!'));


app.get('/hotel', hotel.get);


app.listen(config.app_port, () => console.log('Services listening on port'+ config.app_port+'!'));
